﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkuliatorius
{
    public partial class Kalkuliatorius : Form
    {
        public string X;
        public string operacija;


        public Kalkuliatorius()
        {
            InitializeComponent();
        }

        private void vienas_Click(object sender, EventArgs e)
        {
            Atspausdink("1");
            
        }

        private void du_Click(object sender, EventArgs e)
        {
            Atspausdink("2");
        }

        private void trys_Click(object sender, EventArgs e)
        {
            Atspausdink("3");
        }

        private void keturi_Click(object sender, EventArgs e)
        {
            Atspausdink("4");
        }

        private void penki_Click(object sender, EventArgs e)
        {
            Atspausdink("5");
        }

        private void sesi_Click(object sender, EventArgs e)
        {
            Atspausdink("6");
        }

        private void septyni_Click(object sender, EventArgs e)
        {
            Atspausdink("7");
        }

        private void astuoni_Click(object sender, EventArgs e)
        {
            Atspausdink("8");
        }

        private void devyni_Click(object sender, EventArgs e)
        {
            Atspausdink("9");
        }

        private void nulis_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Contains("0"))
            {
                double x = Convert.ToDouble(Ekranas.Text);
                if(x<1)
                    return;
            }

            Atspausdink("0");
            
               
        }
        public void Atspausdink(string skaicius)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text = Ekranas.Text + skaicius;
            }
        }

        private void plius_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            operacija = "+";

        }
        private void minus_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            operacija = "-";

        }
        private void daugyba_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            operacija = "*";
        }
        private void dalyba_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            operacija = "/";
        }
        private void istrinti_Click(object sender, EventArgs e)
        {
            Ekranas.Clear();
        }
        private void istrintipaskutini_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "") { return; }
            Ekranas.Text = Ekranas.Text.Remove(Ekranas.Text.Length - 1);
        }
        private void zenklas_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "") { return; }
            float a = Convert.ToSingle(Ekranas.Text);
            float b = a * (-1) ;
            string c = Convert.ToString(b);
            Ekranas.Text = c;
        }
        private void kablelis_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "")
            {
                Ekranas.Text = "0"+",";
            }
            else if(Ekranas.Text.Contains(",")) { return; }

            else
            Ekranas.Text = Ekranas.Text + ",";
        }
        private void procentai_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "") { return; }
            float a = Convert.ToSingle(Ekranas.Text);
            float b = a / 100;
            string c = Convert.ToString(b);
            Ekranas.Text = c;
        }
        private void saknis_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "") { return; }
            double x =Convert.ToDouble(Ekranas.Text);
            double y = Math.Sqrt(x);
            string a = Convert.ToString(y);
            Ekranas.Text = a;
        }
        private void lygu_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "" || X == "") { return; }
            float x = Convert.ToSingle(X);
            float y = Convert.ToSingle(Ekranas.Text);
                
            float rezultatas = 0;

            if (operacija == "+")
            {
                rezultatas = x + y;
            }
            else if (operacija == "-")
            {
               rezultatas = x - y;
            }
            else if(operacija == "*")
            {
                rezultatas = x * y;
            }
            else if(operacija == "/")
            {
                rezultatas = x / y;
            }
            
            Ekranas.Text = rezultatas.ToString();
        }

        
    }
}
