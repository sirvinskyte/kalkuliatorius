﻿namespace Kalkuliatorius
{
    partial class Kalkuliatorius
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Kalkuliatorius));
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.vienas = new System.Windows.Forms.Button();
            this.du = new System.Windows.Forms.Button();
            this.trys = new System.Windows.Forms.Button();
            this.keturi = new System.Windows.Forms.Button();
            this.penki = new System.Windows.Forms.Button();
            this.sesi = new System.Windows.Forms.Button();
            this.septyni = new System.Windows.Forms.Button();
            this.astuoni = new System.Windows.Forms.Button();
            this.devyni = new System.Windows.Forms.Button();
            this.nulis = new System.Windows.Forms.Button();
            this.daugyba = new System.Windows.Forms.Button();
            this.dalyba = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.plius = new System.Windows.Forms.Button();
            this.lygu = new System.Windows.Forms.Button();
            this.istrinti = new System.Windows.Forms.Button();
            this.zenklas = new System.Windows.Forms.Button();
            this.kablelis = new System.Windows.Forms.Button();
            this.procentai = new System.Windows.Forms.Button();
            this.istrintipaskutini = new System.Windows.Forms.Button();
            this.saknis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Ekranas.Location = new System.Drawing.Point(50, 37);
            this.Ekranas.Multiline = true;
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(218, 63);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Ekranas.UseWaitCursor = true;
            // 
            // vienas
            // 
            this.vienas.Location = new System.Drawing.Point(50, 274);
            this.vienas.Name = "vienas";
            this.vienas.Size = new System.Drawing.Size(50, 50);
            this.vienas.TabIndex = 1;
            this.vienas.Text = "1";
            this.vienas.UseVisualStyleBackColor = true;
            this.vienas.Click += new System.EventHandler(this.vienas_Click);
            // 
            // du
            // 
            this.du.Location = new System.Drawing.Point(106, 274);
            this.du.Name = "du";
            this.du.Size = new System.Drawing.Size(50, 50);
            this.du.TabIndex = 2;
            this.du.Text = "2";
            this.du.UseVisualStyleBackColor = true;
            this.du.Click += new System.EventHandler(this.du_Click);
            // 
            // trys
            // 
            this.trys.Location = new System.Drawing.Point(162, 274);
            this.trys.Name = "trys";
            this.trys.Size = new System.Drawing.Size(50, 50);
            this.trys.TabIndex = 3;
            this.trys.Text = "3";
            this.trys.UseVisualStyleBackColor = true;
            this.trys.Click += new System.EventHandler(this.trys_Click);
            // 
            // keturi
            // 
            this.keturi.Location = new System.Drawing.Point(50, 218);
            this.keturi.Name = "keturi";
            this.keturi.Size = new System.Drawing.Size(50, 50);
            this.keturi.TabIndex = 4;
            this.keturi.Text = "4";
            this.keturi.UseVisualStyleBackColor = true;
            this.keturi.Click += new System.EventHandler(this.keturi_Click);
            // 
            // penki
            // 
            this.penki.Location = new System.Drawing.Point(106, 218);
            this.penki.Name = "penki";
            this.penki.Size = new System.Drawing.Size(50, 50);
            this.penki.TabIndex = 5;
            this.penki.Text = "5";
            this.penki.UseVisualStyleBackColor = true;
            this.penki.Click += new System.EventHandler(this.penki_Click);
            // 
            // sesi
            // 
            this.sesi.Location = new System.Drawing.Point(162, 218);
            this.sesi.Name = "sesi";
            this.sesi.Size = new System.Drawing.Size(50, 50);
            this.sesi.TabIndex = 6;
            this.sesi.Text = "6";
            this.sesi.UseVisualStyleBackColor = true;
            this.sesi.Click += new System.EventHandler(this.sesi_Click);
            // 
            // septyni
            // 
            this.septyni.Location = new System.Drawing.Point(50, 162);
            this.septyni.Name = "septyni";
            this.septyni.Size = new System.Drawing.Size(50, 50);
            this.septyni.TabIndex = 7;
            this.septyni.Text = "7";
            this.septyni.UseVisualStyleBackColor = true;
            this.septyni.Click += new System.EventHandler(this.septyni_Click);
            // 
            // astuoni
            // 
            this.astuoni.Location = new System.Drawing.Point(106, 162);
            this.astuoni.Name = "astuoni";
            this.astuoni.Size = new System.Drawing.Size(50, 50);
            this.astuoni.TabIndex = 8;
            this.astuoni.Text = "8";
            this.astuoni.UseVisualStyleBackColor = true;
            this.astuoni.Click += new System.EventHandler(this.astuoni_Click);
            // 
            // devyni
            // 
            this.devyni.Location = new System.Drawing.Point(162, 162);
            this.devyni.Name = "devyni";
            this.devyni.Size = new System.Drawing.Size(50, 50);
            this.devyni.TabIndex = 9;
            this.devyni.Text = "9";
            this.devyni.UseVisualStyleBackColor = true;
            this.devyni.Click += new System.EventHandler(this.devyni_Click);
            // 
            // nulis
            // 
            this.nulis.Location = new System.Drawing.Point(50, 330);
            this.nulis.Name = "nulis";
            this.nulis.Size = new System.Drawing.Size(106, 50);
            this.nulis.TabIndex = 10;
            this.nulis.Text = "0";
            this.nulis.UseVisualStyleBackColor = true;
            this.nulis.Click += new System.EventHandler(this.nulis_Click);
            // 
            // daugyba
            // 
            this.daugyba.Location = new System.Drawing.Point(218, 162);
            this.daugyba.Name = "daugyba";
            this.daugyba.Size = new System.Drawing.Size(50, 50);
            this.daugyba.TabIndex = 11;
            this.daugyba.Text = "*";
            this.daugyba.UseVisualStyleBackColor = true;
            this.daugyba.Click += new System.EventHandler(this.daugyba_Click);
            // 
            // dalyba
            // 
            this.dalyba.Location = new System.Drawing.Point(218, 106);
            this.dalyba.Name = "dalyba";
            this.dalyba.Size = new System.Drawing.Size(50, 50);
            this.dalyba.TabIndex = 12;
            this.dalyba.Text = "/";
            this.dalyba.UseVisualStyleBackColor = true;
            this.dalyba.Click += new System.EventHandler(this.dalyba_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(218, 218);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(50, 50);
            this.minus.TabIndex = 13;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // plius
            // 
            this.plius.Location = new System.Drawing.Point(218, 274);
            this.plius.Name = "plius";
            this.plius.Size = new System.Drawing.Size(50, 50);
            this.plius.TabIndex = 14;
            this.plius.Text = "+";
            this.plius.UseVisualStyleBackColor = true;
            this.plius.Click += new System.EventHandler(this.plius_Click);
            // 
            // lygu
            // 
            this.lygu.Location = new System.Drawing.Point(218, 330);
            this.lygu.Name = "lygu";
            this.lygu.Size = new System.Drawing.Size(50, 50);
            this.lygu.TabIndex = 15;
            this.lygu.Text = "=";
            this.lygu.UseVisualStyleBackColor = true;
            this.lygu.Click += new System.EventHandler(this.lygu_Click);
            // 
            // istrinti
            // 
            this.istrinti.Location = new System.Drawing.Point(50, 106);
            this.istrinti.Name = "istrinti";
            this.istrinti.Size = new System.Drawing.Size(50, 50);
            this.istrinti.TabIndex = 16;
            this.istrinti.Text = "C";
            this.istrinti.UseVisualStyleBackColor = true;
            this.istrinti.Click += new System.EventHandler(this.istrinti_Click);
            // 
            // zenklas
            // 
            this.zenklas.Location = new System.Drawing.Point(106, 106);
            this.zenklas.Name = "zenklas";
            this.zenklas.Size = new System.Drawing.Size(50, 50);
            this.zenklas.TabIndex = 17;
            this.zenklas.Text = "-/+";
            this.zenklas.UseVisualStyleBackColor = true;
            this.zenklas.Click += new System.EventHandler(this.zenklas_Click);
            // 
            // kablelis
            // 
            this.kablelis.Location = new System.Drawing.Point(162, 330);
            this.kablelis.Name = "kablelis";
            this.kablelis.Size = new System.Drawing.Size(50, 50);
            this.kablelis.TabIndex = 18;
            this.kablelis.Text = ",";
            this.kablelis.UseVisualStyleBackColor = true;
            this.kablelis.Click += new System.EventHandler(this.kablelis_Click);
            // 
            // procentai
            // 
            this.procentai.Location = new System.Drawing.Point(162, 106);
            this.procentai.Name = "procentai";
            this.procentai.Size = new System.Drawing.Size(50, 50);
            this.procentai.TabIndex = 19;
            this.procentai.Text = "%";
            this.procentai.UseVisualStyleBackColor = true;
            this.procentai.Click += new System.EventHandler(this.procentai_Click);
            // 
            // istrintipaskutini
            // 
            this.istrintipaskutini.Location = new System.Drawing.Point(274, 106);
            this.istrintipaskutini.Name = "istrintipaskutini";
            this.istrintipaskutini.Size = new System.Drawing.Size(50, 50);
            this.istrintipaskutini.TabIndex = 20;
            this.istrintipaskutini.Text = "⌫";
            this.istrintipaskutini.UseVisualStyleBackColor = true;
            this.istrintipaskutini.Click += new System.EventHandler(this.istrintipaskutini_Click);
            // 
            // saknis
            // 
            this.saknis.Location = new System.Drawing.Point(274, 162);
            this.saknis.Name = "saknis";
            this.saknis.Size = new System.Drawing.Size(50, 50);
            this.saknis.TabIndex = 21;
            this.saknis.Text = "√";
            this.saknis.UseVisualStyleBackColor = true;
            this.saknis.Click += new System.EventHandler(this.saknis_Click);
            // 
            // Kalkuliatorius
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 397);
            this.Controls.Add(this.saknis);
            this.Controls.Add(this.istrintipaskutini);
            this.Controls.Add(this.procentai);
            this.Controls.Add(this.kablelis);
            this.Controls.Add(this.zenklas);
            this.Controls.Add(this.istrinti);
            this.Controls.Add(this.lygu);
            this.Controls.Add(this.plius);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.dalyba);
            this.Controls.Add(this.daugyba);
            this.Controls.Add(this.nulis);
            this.Controls.Add(this.devyni);
            this.Controls.Add(this.astuoni);
            this.Controls.Add(this.septyni);
            this.Controls.Add(this.sesi);
            this.Controls.Add(this.penki);
            this.Controls.Add(this.keturi);
            this.Controls.Add(this.trys);
            this.Controls.Add(this.du);
            this.Controls.Add(this.vienas);
            this.Controls.Add(this.Ekranas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Kalkuliatorius";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button vienas;
        private System.Windows.Forms.Button du;
        private System.Windows.Forms.Button trys;
        private System.Windows.Forms.Button keturi;
        private System.Windows.Forms.Button penki;
        private System.Windows.Forms.Button sesi;
        private System.Windows.Forms.Button septyni;
        private System.Windows.Forms.Button astuoni;
        private System.Windows.Forms.Button devyni;
        private System.Windows.Forms.Button nulis;
        private System.Windows.Forms.Button daugyba;
        private System.Windows.Forms.Button dalyba;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button plius;
        private System.Windows.Forms.Button lygu;
        private System.Windows.Forms.Button istrinti;
        private System.Windows.Forms.Button zenklas;
        private System.Windows.Forms.Button kablelis;
        private System.Windows.Forms.Button procentai;
        private System.Windows.Forms.Button istrintipaskutini;
        private System.Windows.Forms.Button saknis;
    }
}

